
var List = (function(){

	var pageConfig = {
			id   : 'page-list',
			name : 'List',
			path : '/list',
			label: 'Liste', // i18n TODO

			// TEMP
			isDefault: true,
		},
		data = null,
		isDataReady = false,

		dataConfigs = {
			sqy: {
				url: 'lib/mobigeo/data/sqypark/db_fr.json',
				field: 'services',
				filterDataToUse: function(_data) {
					return _data[dataConfigs.sqy.field].data;
				},
				getDataToDisplay: function(row) {
					return row[1] + ' - ' + row[2]
				},
			},
			wgc: {
				url: 'lib/wgc/datas.txt',
				field: 'visitors',
				filterDataToUse: function(_data) {
					return _data[dataConfigs.wgc.field].data;
				},
				getDataToDisplay: function(row) {
					return row[1] ? row[1] + '<small>&nbsp;- '+row[2]+'</small>' : null;
				},
			},
		},
		dataConfig = dataConfigs.wgc;

	/**
	 *
	 */
	var fetchData = function() {
		var errorHandler = function() {
			setData([]);
			isDataReady = false;
		};

		fetch(dataConfig.url)
		.then(
			// fetch success
			function(_data) {
				if (_data.status >= 400) {
					Log.warn('Cannot load data file ' + dataConfig.url + ' status is ' + _data.status);
					//throw new Error('Cannot load data file ' + dataConfig.url + ' status is ' + _data.status);
				} else {
					Log.debug('Loaded ' + dataConfig.url);
					return _data.json();
				}
			},
			// fetch failure
			function() {
				console.error('failed to load data', arguments);
				errorHandler();
			}
		)
		.then(
			// parse success
			function(json) {
				if (json) {
					setData(Jt.objectToArray(dataConfig.filterDataToUse(json)));
					isDataReady = true;
				} else {
					errorHandler();
				}
			},
			// parse failure
			function() {
				console.error('failed to parse json', arguments);
				errorHandler();
			}
		);
	};

	var setData = function(value) {
		var timeKey = 'list.update.data'
		console.time(timeKey);

		data = value;
		Pages[pageConfig.name].ractiveInstance.set('elements', value);

		console.timeEnd(timeKey);
	};

	/**
	 * Render the view
	 */
	var render = function() {
		Log.debug('generate List dom');

		if (!Pages[pageConfig.name].ractiveInstance) {
			var ractiveInstance = new Ractive({
				el: Pages.List.el,
				template: '#tplSkeleton',
				data: {
					partial : '#tplList',
					elements: data,
					pages   : Pages,
					toolbarOptions : {
						title: Jt.capitalize(dataConfig.field),
					},
					menuOptions : {
						page : pageConfig.id,
					},
					shouldBeDisplayed: function(row) {
						console.log(row);
						return true;
					},
					getLabel: function(row) {
						return dataConfig.getDataToDisplay(row);
					},
					getOriginalId: function(row) {
						return row[1];
					},
				},
			});
			ractiveInstance.on('list.itemSelected', function(r){ handleElementSelected(r.original.target); });
			Menu.attachListener(ractiveInstance);

			Pages[pageConfig.name].ractiveInstance = ractiveInstance;
		}
	};

    var handleElementSelected = function(el) {
        var listItemEl = el.nodeName === 'LI' ? el : Dt.findParentNodeWithNodeName(el, 'ons-list-item');
        if (listItemEl) {
            var poiId = listItemEl.getAttribute('data-originalid');
            console.log(poiId);
            Router.go(Pages.Map, { poiId: poiId });
        }
    };

	/**
	 * Called just after the view is displayed
	 * @param {object} options
	 */
	var onShow = function(options) {
		Log.debug('on show ' + pageConfig.id);
		if (!isDataReady) {
			fetchData();
		}
	};


	var TestBinding = (function() {
		var toUpperCase = false;

		return {
			run: function() {
				setData(data.map(function(row) {
					if (toUpperCase) {
						row[1] = row[1].toUpperCase();
					} else {
						row[1] = row[1].toLowerCase();
					}
					return row;
				}));
				toUpperCase = !toUpperCase;
			},
		};
	})();


	return {
		get pageConfig() {
			return pageConfig;
		},
		fetchData  : fetchData,
		testBinding: TestBinding.run,
		render     : render,
		onShow     : onShow,
	};
})();
