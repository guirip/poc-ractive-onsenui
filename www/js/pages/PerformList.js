
var PerformList = (function() {

	/**
	 * define the view
	 * @return
	 */
	var view = function() {
		Log.debug('generate PerformList vdom');
		return m('div', { id:'performlist' });
	};

	return {
		view     : view,
	};
})();