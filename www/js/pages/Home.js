
var Home = (function(){

	var pageConfig = {
			id       : 'page-home',
			name     : 'Home',
			path     : '/home',
			label    : 'Accueil (vide)', // i18n TODO
			//isDefault: true,
		};

	/**
	 * define the view
	 * @return vnode
	 */
	var view = function() {
		Log.debug('generate Home vnode');


		// TODO
		return  m('div');
	};

	var onShow = function(options) {
		Log.debug('on show ' + pageConfig.id);
	};

	return {
		get pageConfig() {
			return pageConfig;
		},
		view     : view,
		onShow   : onShow,
	};
})();
