
var Menu = (function() {

	var attachListener = function(ractiveInstance) {
		ractiveInstance.on('menu.toggle', function(r){
			toggle(r.original.target);
		});
		ractiveInstance.on('menu.itemSelected', function(r){
			var targetEl = r.original.target;
            toggle(targetEl);

            var listItemEl,
            	expectedNodeName = 'ONS-LIST-ITEM';
            if (targetEl.nodeName === expectedNodeName) {
            	listItemEl = targetEl;
            } else {
            	listItemEl = Dt.findParentNodeWithNodeName(targetEl, expectedNodeName);
            }
            if (!listItemEl) {
            	Log.warn('Could not find ' + expectedNodeName.toLowerCase() + ' node');
            } else {
            	Router.go(Pages[listItemEl.getAttribute('data-page-id')]);
            }
		});
	};

	var toggle = function(el) {
		Jt.filterOne(Dt.findParentNodeWithNodeName(el, 'ons-splitter').children, { nodeName: 'ONS-SPLITTER-SIDE' }).toggle();
	};

	return {
		attachListener: attachListener,
	};
})();