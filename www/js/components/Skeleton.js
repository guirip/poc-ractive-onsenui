
var Skeleton = (function(){

	var defaultTitle = 'Liste';

	/**
	 * @param  {Vnode} vnode
	 * @param  {object} options
	 * @return {Vnode}
	 */
	var wrap = function(vnode, options) {
		var hasOptions = Jt.isEmpty(options) === false;

		return 	m('ons-page', [
					m('ons-toolbar', [
						m('div.center', hasOptions && options.title ? options.title : defaultTitle),
						m('div.right', [
							m('ons-toolbar-button', { onclick:function(e){ Menu.toggle(e.target); } }, [
								m('ons-icon', { icon:'md-menu', size:'28px' })
							])
						]),
					]),
					vnode,
				]);
	};

	return {
		wrap: wrap,
	};
})();