

var setup = function() {

	Ractive.DEBUG = false;

	// Create pages containers
	iterateOnPages(function(pageKey) {
		var container = document.createElement('div');
		container.setAttribute('id', Pages[pageKey].id);
		container.style.display = 'none';
		Pages[pageKey].el = container;
	});

	// Add these page containers to a main container inside 'body' element
	var pagesContainer = document.createElement('div');
	pagesContainer.setAttribute('id', 'pages-container');
	iterateOnPages(function(key) {
		pagesContainer.appendChild(Pages[key].el);
	});
	document.body.appendChild(pagesContainer);

	Router.run();
};


if (document.readyState === "complete" || document.readyState === "loaded") {
	// Dom ready yet
	setup();
} else {
	// Dom not ready yet
	document.addEventListener('DOMContentLoaded', setup);
}
