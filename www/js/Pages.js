
var Pages = {},
	defaultPage;

_Pages.map(function(component){
	var conf = {
		id       : component.pageConfig.id,
		path     : component.pageConfig.path,
		label    : component.pageConfig.label,
		isMounted: false,
		active   : false,
		component: component,
	};
	if (component.pageConfig.isDefault === true) {
		if (typeof defaultPage !== 'undefined') {
			Log.warn('Mutiple default page...', defaultPage.path, component.pageConfig.path);
		}
		defaultPage = conf;
	}
	Pages[component.pageConfig.name] = conf;
});

var iterateOnPages = function(cb) {
	return Object.keys(Pages).map(cb);
};
