#!/bin/sh
# GR 10/11/2016

bold="\033[1m"
normal="\033[0m"

buildDir=cordova-poc-app
appId="com.mobilespot.pocRactiveOnsenui"
appName="POC-Ractive-OnsenUI"
runTarget=device

log()
{
	echo
	echo $bold$@$normal
}

if [ $# -lt 1 ]
then
	log "Missing platform argument [android/ios]"
	exit 1
fi

if [ -d "${buildDir}" ]
then
	log "Remove existing directory: ${buildDir}"
	rm -Rf $buildDir
fi


log "Create cordova project ["$appId"] ["$appName"]"
cordova create $buildDir $appId $appName && \
cd $buildDir && \


log "Clean www/ directory and copy poc app" && \
rm -Rf www && \
cp -RL ../www . && \


log "Build app" && \
cordova platform add $1 && \
cordova build && \


log "Run app on ${runTarget}" && \
cordova run --$runTarget


status=$?
if [ $? -gt 0 ]
then
	log "Failure"
fi
echo
exit $status
